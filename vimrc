"pathogen calls should be at the top of the file
execute pathogen#infect() 
execute pathogen#helptags()


"have the html completion script load for html, xml xsl files
au Filetype html,xml,xsl source ~/.vim/scripts/closetag.vim



"Incremental searching, searches as you type
set clipboard=unnamedplus "this allows yanks to go to the clipboard. Make sure :echo has('clipboard') does not echo 0

"improve searching
set incsearch	"Incremental searching, searches as you type
set ignorecase "search ignoring case
set smartcase "smartcase means we only ignore case if the entire search string is lowercase

"improve tabs and indenting
set tabstop=4	" tabstop sets the number of spaces a tab should take up
"set shiftwidth=4	" controls depth of autoindentation
set expandtab	" converts all tabs to spaces
set wildmode=longest,list "adds filenmae autocompletion the same way the linux operating system works 
set scrolloff=10 "have at least 10 lines between cursor and bottom while scrolling
set wrap
"set textwidth=79 wrapmargin=0
" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  "set hlsearch
endif


"improve status line to tell us the file type
set statusline=%F%m%r%h%w\ [FORMAT=%{&ff}]\ [TYPE=%Y]\ [POS=%l,%v][%p%%]\ %{strftime(\"%d/%m/%y\ -\ %H:%M\")}

set laststatus=2 "always display statusbar

"enable filetype plugin and indentation, this should allow different filetypes
"to handle indentation differently. 
filetype plugin on
filetype indent on

"enable omnicomplete, to use, type <C-X><C-O>, navigate with <C-N><C-P> see :help compl-omni
"set omnifunc=syntaxcomplete#Complete

"key remap, we can jump between splits with ctrl-[h,j,k,l] (the letter is the
"direction you want to jump
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>


"set colorscheme
colorscheme torte

"change <Leader> key to comma
let mapleader=","


"map F9 to TagbarOpenAutoClose, this opens the Tagbar window, jumps to it and
"closes it on tag selection
nnoremap <silent> <F9> :TagbarOpenAutoClose<CR>

"map F8 to Open Nerdtree
nnoremap <silent> <F8> :NERDTree<CR>

"map ctrl-p to :CtrlPBuffer
nnoremap <C-b> :CtrlPBuffer<CR>

"set the forward and back jump triggers in ultisnips be ctrl[h,l]
let g:UltiSnipsJumpForwardTrigger="<C-L>"
let g:UltiSnipsJumpBackwardTrigger="<C-H>"

"remap tab and shift tab to tab or reverse tabs in command mode:
nmap <S-Tab> <<
nmap <Tab> >>

"these lines are just for test from http://superuser.com/questions/399296/256-color-support-for-vim-background-in-tmux 
set t_ut=
set t_Co=256

"remove all trailing whitespaces on save for programming filetypes:
autocmd FileType c,cpp,java,php,python,yaml autocmd BufWritePre <buffer> :%s/\s\+$//e

"enable mouse:
set mouse=a

"have jedi open goto in buffers, not tabs:
let g:jedi#use_tabs_not_buffers=0

"remap bp bd# which closes the current buffer in the current window split
"without closing the window split. See:
"http://stackoverflow.com/questions/4465095/vim-delete-buffer-without-losing-the-split-window
nnoremap <C-c> :bp\|bd #<CR>

"detect csv files
au BufNewFile,BufRead *.csv set filetype=csv
